Contributions are welcome.

Please:

 1. Raise an issue describing what you would like to do
 2. @jonty in the issue when ready to discuss
 3. If result of discussion is to add code / docs, then please:
    
    - Branch this repo
    - Raise an MR and assign to @jonty

Enjoy!